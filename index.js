const express = require("express");
const axios = require("axios");
const cors = require("cors");

const CLIENT_ID = "jaqrpTf4wUuoA8VNxg9_";
const CLIENT_SECRET = "B2IUNA16Hi6cSfioMBWBjp-Q3WkwqJGa8uLQOh0mjNgme5RpqJnIMw==";
const GITHUB_URL = "https://api.loyverse.com/oauth/authorize";

const app = express();


const corsOptions = {
  origin: ['http://localhost:3000'],
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions));

app.get("/oauth/redirect", (req, res) => {
  axios({
    method: "POST",
    url: `${GITHUB_URL}?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&code=${req.query.code}`,
    headers: {
      Accept: "application/json",
    },
  }).then((response) => {
    res.redirect(
      `http://localhost:3000?access_token=${response.data.access_token}`
    );
  });
});

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening at port ${PORT}`);
});